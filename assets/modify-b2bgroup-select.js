console.log('apmmust_vendor_b2bking_variables', apmmust_vendor_b2bking_variables);

if (apmmust_vendor_b2bking_variables && apmmust_vendor_b2bking_variables['b2bking_vendor_group_id']) {
  $id = apmmust_vendor_b2bking_variables['b2bking_vendor_group_id'];
  document.addEventListener('DOMContentLoaded', function () {
    const $select = document.querySelector('.b2bking_user_registration_user_data_container_element_select_group');
    if ($select === null) return;

    // Loop through all the options in the <select>
    for (var i = 0; i < $select.options.length; i++) {
      // Get the current option
      const option = $select.options[i];
      console.log('option', option);

      // Check if the option's value is '3871517'
      if (option.value == $id) {
        // If it is, select this option and break the loop
        $select.selectedIndex = i;
        break;
      }
    }
  });
}
