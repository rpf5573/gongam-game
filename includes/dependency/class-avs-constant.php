<?php

class AVS_Constant {
  const METABOX_USER_PASSWORD = 'team_password';
  const POST_TYPE_QUIZ = 'quiz';
  const METABOX_SETTING_PAGE = 'gongam_setting_page';
  const METABOX_GAME_STATUS_SETTING_PAGE = 'gongam_game_status_setting_page';
  const METABOX_SETTING_PAGE_ACTIVE_TEAM = 'active_team';
  const METABOX_QUIZ_MAIN_TEAM = 'main_team';
  const METABOX_QUIZ_MAIN_ANSWER = 'main_team_answer';
  const METABOX_TEAM_MEMBER_COUNT = 'team_member_count';
  const METABOX_TEAM_TOTAL_POINT = 'team_total_point';
  const METABOX_LOGO = 'logo';
  const METABOX_GAME_STATUS = 'game_status';

  const METABOX_QUIZ_SUB_ANSWER = 'team_sub_answer';
  const METABOX_QUIZ_SUB_ANSWER_1 = 'team_sub_answer_1';
  const METABOX_QUIZ_SUB_ANSWER_2 = 'team_sub_answer_2';
  const METABOX_QUIZ_SUB_ANSWER_3 = 'team_sub_answer_3';
  const METABOX_QUIZ_SUB_ANSWER_4 = 'team_sub_answer_4';
  const METABOX_QUIZ_SUB_ANSWER_5 = 'team_sub_answer_5';
  const METABOX_QUIZ_SUB_ANSWER_6 = 'team_sub_answer_6';
  const METABOX_QUIZ_SUB_ANSWER_7 = 'team_sub_answer_7';
  const METABOX_QUIZ_SUB_ANSWER_8 = 'team_sub_answer_8';
  const METABOX_QUIZ_SUB_ANSWER_9 = 'team_sub_answer_9';
  const METABOX_QUIZ_SUB_ANSWER_10 = 'team_sub_answer_10';
  const METABOX_QUIZ_SUB_ANSWER_11 = 'team_sub_answer_11';
  const METABOX_QUIZ_SUB_ANSWER_12 = 'team_sub_answer_12';
  const METABOX_QUIZ_SUB_ANSWER_13 = 'team_sub_answer_13';
  const METABOX_QUIZ_SUB_ANSWER_14 = 'team_sub_answer_14';
  const METABOX_QUIZ_SUB_ANSWER_15 = 'team_sub_answer_15';
  const METABOX_QUIZ_SUB_ANSWER_16 = 'team_sub_answer_16';
  const METABOX_QUIZ_SUB_ANSWER_17 = 'team_sub_answer_17';
  const METABOX_QUIZ_SUB_ANSWER_18 = 'team_sub_answer_18';
  const METABOX_QUIZ_SUB_ANSWER_19 = 'team_sub_answer_19';
  const METABOX_QUIZ_SUB_ANSWER_20 = 'team_sub_answer_20';

}
