<?php

class AVS_Meta_Boxes {
  public function __construct(AVS_Loader $loader) {
    $this->loader = $loader;

    // 우선순위를 낮춰야 한다
    $this->loader->add_action('profile_update', $this, 'update_custom_field_on_password_change', 100, 2);
    $this->loader->add_filter('mb_settings_pages', $this, 'setting_page');
    
    $this->loader->add_filter('rwmb_meta_boxes', $this, 'custom_field_user_password' );
    $this->loader->add_filter('rwmb_meta_boxes', $this, 'custom_fields_setting_page' );
    $this->loader->add_filter( 'rwmb_meta_boxes', $this, 'custom_fields_user' );
    $this->loader->add_filter( 'rwmb_meta_boxes', $this, 'custom_field_logo' );

    // $this->loader->add_action('mb_settings_page_load', $this, 'game_status_changed', 1000);
  }

  // Function to update custom field when a user's password is updated
  public function update_custom_field_on_password_change($user_id, $old_user_data) {
    if (!isset($_POST['pass1']) || empty($_POST['pass1'])) {
      return;
    }
    $user = get_userdata($user_id);
    $new_pw = $_POST['pass1'];

    rwmb_set_meta( $user_id, AVS_Constant::METABOX_USER_PASSWORD, $new_pw, [ 'object_type' => 'user' ] );
  }

  public function custom_field_user_password( $meta_boxes ) {
    $meta_boxes[] = [
      'title'  => '팀별 비밀번호',
      'id'     => 'team_password_field_group_id',
      'type'   => 'user',
      'fields' => [
        [
          'name'              => __( '팀비밀번호', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_USER_PASSWORD,
          'type'              => 'text',
          'label_description' => __( '현재 팀의 비밀번호입니다', 'your-text-domain' ),
          'readonly'          => true,
        ],
      ],
    ];
    return $meta_boxes;
  }

  public function setting_page( $settings_pages ) {
    $settings_pages[] = [
      'menu_title' => '공감 게임 설정',
      'id'         => AVS_Constant::METABOX_SETTING_PAGE,
      'position'   => 26,
      'style'      => 'no-boxes',
      'columns'    => 1,
      'icon_url'   => 'dashicons-admin-settings',
    ];

    $settings_pages[] = [
      'menu_title' => '공감 게임 상태 설정',
      'id'         => AVS_Constant::METABOX_GAME_STATUS_SETTING_PAGE,
      'position'   => 26,
      'style'      => 'no-boxes',
      'columns'    => 1,
      'icon_url'   => 'dashicons-admin-settings',
    ];
  
    return $settings_pages;
  }

  public function custom_fields_setting_page( $meta_boxes ) {
    $meta_boxes[] = [
      'title'          => '공감 게임 설정 옵션',
      'id'             => 'gongam_game_setting_option_field_group_id',
      'settings_pages' => [AVS_Constant::METABOX_SETTING_PAGE],
      'fields'         => [
        [
          'name'              => '현재 주인공 팀 지정',
          'id'                => AVS_Constant::METABOX_SETTING_PAGE_ACTIVE_TEAM,
          'type'              => 'user',
          'label_description' => '현재 주인공인 팀을 설정합니다',
          'field_type'        => 'radio_list',
          'query_args' => [
            'role__not_in' => 'administrator',
          ],
        ],
      ],
    ];

    return $meta_boxes;
  }

  public function custom_fields_user( $meta_boxes ) {
    $meta_boxes[] = [
      'title'  => __( '팀별 정보', 'your-text-domain' ),
      'id'     => 'team_info_field_group_id',
      'type'   => 'user',
      'fields' => [
        [
          'name' => __( '총 점수', 'your-text-domain' ),
          'id'   => AVS_Constant::METABOX_TEAM_TOTAL_POINT,
          'type' => 'number',
        ],
        [
          'name'              => __( '팀인원수', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_TEAM_MEMBER_COUNT,
          'type'              => 'number',
          'label_description' => __( '팀 인원수를 입력해주세요', 'your-text-domain' ),
          'min'               => 1,
          'max'               => 20,
          'step'              => 1,
          'std'               => 10,
        ],
        [
          'name'              => __( '주인공팀 답변', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_MAIN_ANSWER,
          'type'              => 'text',
          'label_description' => __( '주인공이 입력한 답변입니다', 'your-text-domain' ),
        ],
        [
          'name'              => __( '관객팀 답변 - 1', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_1,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 2', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_2,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 3', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_3,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 4', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_4,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 5', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_5,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 6', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_6,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 7', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_7,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 8', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_8,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 9', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_9,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 10', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_10,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 11', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_11,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 12', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_12,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 13', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_13,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 14', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_14,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 15', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_15,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 16', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_16,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 17', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_17,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 18', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_18,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 19', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_19,
          'type'              => 'text',
        ],
        [
          'name'              => __( '관객팀 답변 - 20', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_QUIZ_SUB_ANSWER_20,
          'type'              => 'text',
        ],
      ],
    ];
  
    return $meta_boxes;
  }

  public function custom_field_logo( $meta_boxes ) {
    $meta_boxes[] = [
      'title'          => __( '로고 이미지 지정', 'your-text-domain' ),
      'id'             => 'logo_image_group_id',
      'settings_pages' => [AVS_Constant::METABOX_SETTING_PAGE],
      'fields'         => [
        [
          'name'              => __( '회사 로고', 'your-text-domain' ),
          'id'                => AVS_Constant::METABOX_LOGO,
          'type'              => 'single_image',
          'label_description' => __( '로고 이미지를 넣어주세요', 'your-text-domain' ),
          'required'          => true,
        ],
      ],
    ];

    return $meta_boxes;
  }

  public function game_status_changed() {
    $key = AVS_Constant::METABOX_GAME_STATUS;
    if (isset($_POST[$key])) {
      $status = $_POST[$key];
      if ($status === 'reset') {
        gongam_reset();
      }
    }
  }
}
