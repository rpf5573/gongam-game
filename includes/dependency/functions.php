<?php

function gongam_find_email_by_password($password) {
  global $wpdb; // Global WP database object.

  $meta_key = AVS_Constant::METABOX_USER_PASSWORD;

  $query = "
    SELECT u.user_email
    FROM {$wpdb->prefix}users AS u
    JOIN {$wpdb->prefix}usermeta AS um ON u.ID = um.user_id
    WHERE um.meta_key = '{$meta_key}' AND um.meta_value = %s
  ";

  $email = $wpdb->get_var($wpdb->prepare($query, $password));

  return $email ? $email : false;
}

function gongam_error_response($error_code = null, $error_message = '', $dev_error_message = '', $status_code = 400, $data = null) {
  $response = array(
    'code' => $error_code,
    'message' => $error_message,
    'dev_error_message' => $dev_error_message,
    'data' => $data
  );

  wp_send_json( $response, $status_code );
}

function gongam_success_response($success_code = null, $success_message = '', $data = null, $status_code = 200) {
  $response = array(
    'code' => $success_code,
    'message' => $success_message,
    'data' => $data
  );
  wp_send_json( $response, $status_code );
}

function gongam_get_user_email_by_id($user_id) {
  $user = get_userdata($user_id);

  if ($user) {
    return $user->user_email;
  }

  return null; // Yep, user doesn't exist or no email.
}

function gongam_get_active_team() {
  $active_user_id = rwmb_meta( AVS_Constant::METABOX_SETTING_PAGE_ACTIVE_TEAM, ['object_type' => 'setting'],  AVS_Constant::METABOX_SETTING_PAGE );
  return $active_user_id;
}

// 2 => 1팀
function gongam_set_active_team($user_id = 2) {
  rwmb_set_meta(AVS_Constant::METABOX_SETTING_PAGE, AVS_Constant::METABOX_SETTING_PAGE_ACTIVE_TEAM, $user_id, ['object_type' => 'setting']);
}

function gongam_get_main_answer_of_team($team) {
  $answer = rwmb_meta(AVS_Constant::METABOX_QUIZ_MAIN_ANSWER, ['object_type' => 'user'], $team);
  return $answer;
}

// team = user_id
function gongam_set_main_answer_to_team($team, $answer) {
  return rwmb_set_meta($team, AVS_Constant::METABOX_QUIZ_MAIN_ANSWER, $answer, ['object_type' => 'user'] );
}

// team = user_id
function gongam_add_sub_answer_to_team($team, $answer) {
  global $wpdb;

  $table = "{$wpdb->prefix}usermeta";

  // Loop through the meta keys and check if a row exists for each one. If not, insert it.
  for ($i = 1; $i <= 20; $i++) {
      $meta_key = AVS_Constant::METABOX_QUIZ_SUB_ANSWER . "_{$i}";

      $exists = $wpdb->get_var($wpdb->prepare(
          "SELECT COUNT(*) FROM $table WHERE meta_key = %s AND user_id = %d", 
          $meta_key, 
          $team
      ));

      // If the row doesn't exist, insert it.
      if (!$exists) {
          $wpdb->insert(
              $table, 
              [
                  'user_id' => $team,
                  'meta_key' => $meta_key,
                  'meta_value' => $answer
              ]
          );
          break; // STOP
      }
  }
}

function gongam_reset_sub_answer_of_team($team) {
  global $wpdb;

  $table = "{$wpdb->prefix}usermeta";

  // Loop through the meta keys and check if a row exists for each one. If not, insert it.
  for ($i = 1; $i <= 20; $i++) {
    $meta_key = AVS_Constant::METABOX_QUIZ_SUB_ANSWER . "_{$i}";

    // remove row where meta_key is $meta_key and user_id is $team
    $wpdb->delete($table, array('meta_key' => $meta_key, 'user_id' => $team));
  }
}

function gongam_get_count_of_sub_answer($team) {
  $list = gongam_get_all_sub_answer_list($team);
  $count = count($list);

  return $count;
}

function gongam_get_all_sub_answer_list($team) {
  global $wpdb;
  
  $table = "{$wpdb->prefix}usermeta";
  $base_field_id = AVS_Constant::METABOX_QUIZ_SUB_ANSWER;

  // Create an array for field_ids from team_sub_answer_1 to team_sub_answer_20
  $field_ids = [];
  for ($i = 1; $i <= 20; $i++) {
    $field_ids[] = $base_field_id . '_' . $i;
  }

  // Create placeholders for each of the field_ids
  $placeholders = implode(', ', array_fill(0, count($field_ids), '%s'));

  // Prepare the SQL statement
  $sql = "SELECT `meta_value` FROM $table WHERE `meta_key` IN ($placeholders) AND user_id = '{$team}'";
  $query = $wpdb->prepare($sql, ...$field_ids);

  $results = $wpdb->get_col($query);

  // Filter out empty values
  $filtered_results = array_filter($results, function($value) {
    return !empty($value);
  });

  // Get the count of the filtered results
  return $filtered_results;
}

function gongam_get_user_ids_except_admins() {
  $args = array(
    'role__not_in' => array('administrator'), // Sorry, administrators, not today!
    'fields' => 'ID'
  );
  
  $users = get_users($args);
  return $users;
}

function gongam_get_user_display_name_by_id($user_id) {
  $user = get_userdata($user_id);
  
  if ($user) { // Check if the user exists.
    return $user->display_name;
  }
  
  return ''; // In case the user ID doesn't exist or there's an error.
}

function gongam_get_max_team_member_count() {
  $user_ids = gongam_get_user_ids_except_admins();
  $field_id = AVS_Constant::METABOX_TEAM_MEMBER_COUNT;
  $max_team_count = array_reduce($user_ids, function($carry, $team_id) use ($field_id) {
    $member_count = rwmb_meta($field_id, array('object_type' => 'user'), $team_id);
    if ($carry < $member_count) {
      return $member_count;
    }
    return $carry;
  }, 8);
  return $max_team_count;
}

function gongam_get_team_member_count($team_id) {
  $member_count = rwmb_meta($field_id, array('object_type' => 'user'), $team_id);
  return empty($member_count) ? 10 : $member_count;
}

function gongam_strip_all_space($str) {
  return preg_replace('/\s+/', '', $str);
}

function gongam_get_logo() {
  $logo = rwmb_meta( AVS_Constant::METABOX_LOGO, ['object_type' => 'setting'],  AVS_Constant::METABOX_SETTING_PAGE );
  if (isset($logo['full_url'])) {
    return $logo['full_url'];
  }
  return '';
}

function gongam_get_game_status() {
  $game_status = rwmb_meta( AVS_Constant::METABOX_GAME_STATUS, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_STATUS_SETTING_PAGE );
  return $game_status;
}

function gongam_set_game_status($game_status) {
  rwmb_set_meta(AVS_Constant::METABOX_GAME_STATUS_SETTING_PAGE, AVS_Constant::METABOX_GAME_STATUS, $game_status, ['object_type' => 'setting']);
}

function gongam_remove_all_sub_answers() {
  $user_ids = gongam_get_user_ids_except_admins();
  foreach($user_ids as $user_id) {
    gongam_reset_sub_answer_of_team($user_id);
  }
}

function gongam_remove_all_main_answers() {
  $user_ids = gongam_get_user_ids_except_admins();
  foreach($user_ids as $user_id) {
    gongam_set_main_answer_to_team($user_id, null);
  }
}

function gongam_answer_reset() {
  gongam_remove_all_main_answers();
  gongam_remove_all_sub_answers();
}

function gongam_all_data_as_html() {
  // Fetching data for the active team
  $active_team = gongam_get_active_team();

  // Getting main answer for the active team
  $main_answer = gongam_get_main_answer_of_team($active_team);
  $striped_main_answer = gongam_strip_all_space($main_answer);
  // Getting all team ids except admins
  $all_team_ids = gongam_get_user_ids_except_admins();
  // Fetching sub answers for each team and organizing them by team
  $all_sub_answers_group_by_team = array_reduce($all_team_ids, function($list, $team_id) {
    $display_name = gongam_get_user_display_name_by_id($team_id);
    $sub_answers = gongam_get_all_sub_answer_list($team_id);
    if (count($sub_answers) > 0) {
      $list[$display_name ? $display_name : $team_id] = gongam_get_all_sub_answer_list($team_id);
    }
    return $list;
  }, []);

  $max_team_member_count = gongam_get_max_team_member_count();

  ob_start();
  ?>
  
  <div class="all-data-html">
    <div class="px-4 pt-1">
        <h1 class="main-answer text-center text-[80px] mb-[15vh] font-bold pt-[15vh] item">
            <span class="p-4">
                <?php echo $main_answer ?>
            </span>
        </h1>

        <div class="flex justify-center gap-4 w-full">  <?php
        // Iterating over grouped sub-answers by team
        foreach ($all_sub_answers_group_by_team as $team => $answers):  ?>
            <div class="item" style="flex-basis: calc(100%/6);">
                <table class="ui celled table !rounded-2xl overflow-hidden">
                    <thead>
                        <!-- Table header row with the team name -->
                        <tr><th colspan="2" class="text-[18px]"><?php echo $team ?></th></tr>
                    </thead>
                    <tbody> 
                        <?php
                        // Create an array iterator to fetch two answers at a time
                        $answersIterator = new ArrayIterator($answers);

                        // Iterating over each pair of answers in the current team's answers
                        while ($answersIterator->valid()): 
                            // Fetch the first answer
                            $firstAnswer = $answersIterator->current();
                            $firstStripedAnswer = gongam_strip_all_space($firstAnswer);
                            $answersIterator->next();
                            
                            // Fetch the second answer
                            $secondAnswer = $answersIterator->current();
                            $secondStripedAnswer = gongam_strip_all_space($secondAnswer);
                            $answersIterator->next();
                        ?>
                            <tr>
                                <td class="<?php echo !empty($firstAnswer) && $firstStripedAnswer === $striped_main_answer ? 'positive-ready' : '' ?> text-[18px]">
                                    <?php echo isset($firstAnswer) && $firstStripedAnswer === $striped_main_answer ? '<i class="icon checkmark"></i>' : '' ?>
                                    <?php echo isset($firstAnswer) ? $firstAnswer : '-' ?>
                                </td>
                                
                                <td class="<?php echo !empty($secondAnswer) && $secondStripedAnswer === $striped_main_answer ? 'positive-ready' : '' ?> text-[18px]">
                                    <?php echo isset($secondAnswer) && $secondStripedAnswer === $striped_main_answer ? '<i class="icon checkmark"></i>' : '' ?>
                                    <?php echo isset($secondAnswer) ? $secondAnswer : '-' ?>
                                </td>
                            </tr> 
                        <?php 
                        endwhile; 
                        ?>
                    </tbody>
                </table>
            </div>  <?php 
        endforeach; 
        ?>
        </div>
    </div>
  </div>
  <?php
  return ob_get_clean();
}

