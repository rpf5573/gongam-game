<?php

class AVS_Rest_Game_Settings {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/game-settings';

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route( $this->namespace, 'post-game-status', [
      'methods'       => 'POST',
      'callback'      => function(WP_REST_Request $request) {
        $game_status = $request->get_param('game_status');
        if ($game_status === 'running') {
          gongam_set_game_status('running');
          gongam_answer_reset();
        }
        else if ($game_status === 'ready') {
          gongam_set_game_status('ready');
        }

        return gongam_success_response('success_post_game_status', '성공적으로 게임 상태를 변경했습니다', array());
      },
      'args'          => array(
        'game_status' => array(
          'required' => true,
          'type' => 'string',
          'description' => '게임 상태를 변경합니다',
        ),
      ),
    ]);

    register_rest_route($this->namespace, 'get-game-status', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request) {
        $game_status = gongam_get_game_status();

        return gongam_success_response( 'success_get_game_status', '성공적으로 게임 설정 값을 가져왔습니다', array(
          'game_status' => $game_status,
        ) );
      },

      // @TODO: 관리자만 가져올 수 있도록 추후에 개선한다
      'permission_callback' => function() {
        return true;
      },
    ]);

    register_rest_route($this->namespace, 'get-all-data-html', [
      'methods'            => 'GET',
      'callback'           => function(WP_REST_Request $request) {
        $html = gongam_all_data_as_html();
        return gongam_success_response( 'success_get_all_data_html', '성공적으로 게임 설정 값을 가져왔습니다', $html);
      },
    ]);
  }

  public function get_total_word_count() {
    $total_word_count = rwmb_meta( AVS_Constant::METABOX_TOTAL_WORD_COUNT, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_SETTING_PAGE );
    return $total_word_count;
  }

  public function get_rail_distance() {
    $rail_distance = rwmb_meta( AVS_Constant::METABOX_RAIL_DISTANSE, ['object_type' => 'setting'],  AVS_Constant::METABOX_GAME_SETTING_PAGE );
    return $rail_distance;
  }

  public function get_speed_per_word() {
    return racing_get_speed_per_word();
  }

  public function get_team_count() {
    return racing_get_team_count();
  }
}
