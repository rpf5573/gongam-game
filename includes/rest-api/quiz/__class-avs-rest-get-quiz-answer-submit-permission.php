<?php

class AVS_Rest_Get_Quiz_ {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/quiz'; // signup은 namespace가 없다

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route($this->namespace, 'get-quiz-answer-submit-permission', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request){
        $quiz_id = $request->get_param('quiz_id');
        $main_team = rwmb_meta(AVS_Constant::METABOX_QUIZ_MAIN_TEAM, ['object_type' => 'post', 'post_type' => AVS_Constant::POST_TYPE_QUIZ], $quiz_id);

        return gongam_success_response( 'success_get_quiz_detail', '성공적으로 퀴즈를 불러왔습니다', $quiz );
      },
      'args' => [
        'quiz_id' => [
          'required' => true,
          'type' => 'integer',
          'description' => '퀴즈 아이디',
          'validation_callback' => function($param, $request, $key) {
            if (!is_numeric($param)) {
              return new WP_Error('rest_invalid_param', 'quiz_id는 숫자여야 합니다');
            }
            return true;
          },
        ],
      ],
      'permission_callback' => function() {
        return get_current_user_id() > 0;
      },
    ]);
  }
}
