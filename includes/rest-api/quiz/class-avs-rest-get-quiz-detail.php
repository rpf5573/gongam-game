<?php

class AVS_Rest_Get_Quiz_Detail {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/quiz'; // signup은 namespace가 없다

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route($this->namespace, 'get-quiz-detail', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request){
        $quiz_id = $request->get_param('quiz_id');
        $user_id = strval(get_current_user_id());

        $main_team_user_id = gongam_get_main_team_of_quiz($quiz_id);
        if (empty($main_team_user_id)) {
          return gongam_error_response( 'empty_main_team', '메인팀이 설정되어 있지 않습니다. 관리자에게 문의해주세요', '', 404 );
        }
        $main_team_user_email = gongam_get_user_email_by_id($main_team_user_id);
        if (empty($main_team_user_email)) {
          return gongam_error_response( 'empty_main_team_user_email', '메인팀의 이메일이 설정되어 있지 않습니다. 관리자에게 문의해주세요', '', 404 );
        }

        $main_answer = '';
        // 현재 로그인한 사용자가 이 퀴즈의 주인공이라면
        if ($user_id === strval($main_team_user_id)) {
          $main_answer = gongam_get_main_answer_of_quiz($quiz_id);
        }

        $args = array(
          'post_type' => AVS_Constant::POST_TYPE_QUIZ,
          'p' => $quiz_id,
          'post_status' => 'publish'
        );

        $query = new WP_Query($args);
        $quiz = array();

        // 현재 퀴즈가 비활성 퀴즈라면, content는 비워둔다
        $active_quiz_id = strval(gongam_get_active_quiz_id());

        if ($query->have_posts()) {
          while ($query->have_posts()) {
            $inactive = empty($active_quiz_id) || strval($quiz_id) !== $active_quiz_id;
            $content = $inactive ? '아직 이 퀴즈를 풀 차례가 아닙니다' : get_the_content();
            $query->the_post();
            $quiz = array(
              'id' => get_the_ID(),
              'title' => get_the_title(),
              'content' => $content,
              'author' => get_the_author(),
              'created_at' => get_the_date('Y-m-d'),
              'updated_at' => get_the_modified_date('Y-m-d'),
              'main_team_user_id' => $main_team_user_id,
              'main_team_user_email' => $main_team_user_email,
              'main_answer' => $main_answer,
              'active' => !$inactive,
            );
          }
          wp_reset_postdata();
        } else {
          return gongam_error_response( 'error_get_quiz_detail', '퀴즈를 불러오는데 실패했습니다', '', 404 );
        }

        return gongam_success_response( 'success_get_quiz_detail', '성공적으로 퀴즈를 불러왔습니다', $quiz );
      },
      'args' => [
        'quiz_id' => [
          'required' => true,
          'type' => 'integer',
          'description' => '퀴즈 아이디',
          'validation_callback' => function($param, $request, $key) {
            if (!is_numeric($param)) {
              return new WP_Error('rest_invalid_param', 'quiz_id는 숫자여야 합니다');
            }
            return true;
          },
        ],
      ],
      'permission_callback' => function() {
        return get_current_user_id() > 0;
      },
    ]);
  }
}
