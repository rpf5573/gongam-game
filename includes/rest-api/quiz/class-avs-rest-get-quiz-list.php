<?php

class AVS_Rest_Get_Quiz_List {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/quiz'; // signup은 namespace가 없다

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    register_rest_route($this->namespace, 'get-quiz-list', [
      'methods'             => 'GET',
      'callback'            => function(WP_REST_Request $request){
        $args = array(
          'post_type' => AVS_Constant::POST_TYPE_QUIZ,
          'paged' => 1,
          'posts_per_page' => 999,
        );

        $query = new WP_Query($args);
        $quiz_list = array();

        if ($query->have_posts()) {
          while ($query->have_posts()) {
            $query->the_post();
            $quiz_list[] = array(
              'id' => get_the_ID(),
              'title' => get_the_title(),
              'content' => get_the_content(),
              'author' => get_the_author(),
              'created_at' => get_the_date('Y-m-d'),
              'updated_at' => get_the_modified_date('Y-m-d'),
            );
          }
          wp_reset_postdata();
        }

        $data = [
          'quiz_list' => $quiz_list,
          'total_count' => $query->found_posts,
          'current_page' => $page,
        ];
        return gongam_success_response( 'success_get_quiz_list', '성공적으로 퀴즈 목록을 불러왔습니다', $data );
      },
      'permission_callback' => function() {
        return get_current_user_id() > 0;
      },
    ]);
  }
}
