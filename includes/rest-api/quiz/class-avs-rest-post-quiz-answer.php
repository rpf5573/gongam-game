<?php

class AVS_Rest_Post_Quiz_Answer {
  private string $namespace;

  protected AVS_Loader $loader;

  public function __construct($namespace, AVS_Loader $loader) {
    $this->loader = $loader;
    $this->namespace   = $namespace . '/quiz'; // signup은 namespace가 없다

    $this->loader->add_action('rest_api_init', $this, 'add_api_routes');
  }

  public function add_api_routes() {
    // @TODO: 넘어온 값들이 실제로 term, attribute에 존재하는지 확인해야 한다
    register_rest_route($this->namespace, 'submit-quiz-answer', [
      'methods'             => 'POST',
      'callback'            => function(WP_REST_Request $request){
        $user_id = strval(get_current_user_id());
        $answer = $request->get_param('answer');
        $active_team = gongam_get_active_team();

        $game_status = gongam_get_game_status();
        if ($game_status !== 'running') {
          return gongam_error_response('game_not_running', '아직 게임이 시작되지 않았습니다');
        }

        // 메인팀인데
        if ($user_id === strval($active_team)) {
          $main_answer = gongam_get_main_answer_of_team($active_team);
          if (!empty($main_answer)) {
            return gongam_error_response('already_submited_main_answer', '추가로 입력할 수 없습니다');
          }

          $result = gongam_set_main_answer_to_team($active_team, $answer);
          return gongam_success_response('updated_main_answer', '입력 완료');
        }
        // 서브팀이라면
        else {
          $count = gongam_get_count_of_sub_answer($user_id);
          $team_member_count = gongam_get_team_member_count($user_id);
          
          if ($count >= 20) {
            return gongam_error_response('already_full_sub_answer', '추가로 입력할 수 없습니다');
          }

          $result = gongam_add_sub_answer_to_team($user_id, $answer);
          return gongam_success_response('updated_sub_answer', '입력 완료');
        }
      },
      'args' => [
        'answer' => [
          'required' => false,
          'validate_callback' => function($param, $request, $key) {
            function_exists('ray') && ray('param', $param);
            if (empty($param)) {
              return new WP_Error( 'empty_answer', '답변이 비어있습니다' );
            }
            return true;
          },
        ],
      ],
      'permission_callback' => function() {
        $id = get_current_user_id();
        function_exists('ray') && ray('id ', $id);
        return $id > 0;
      },
    ]);
  }
}
