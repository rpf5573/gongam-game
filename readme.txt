=== APMMUST Vendor Plugin ===

Contributors: airman5573
Tags: wp-json, jwt, json web authentication, wp-api, woocommerce
Requires at least: 6.0.3
Tested up to: 6.1.1
Requires PHP: 7.4.0
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Extends the WP REST API using JSON Web Tokens Authentication as an authentication method.

== Description ==

APMMUST Vendor 기능을 위한 플러그인입니다.

***This plugins copy codes of [JWT Authentication for WP REST API](https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/)

## 주요 기능
### REST API
- JWT 기반 로그인
- 상품 생성
- 상품 목록 불러오기
- 주문 목록 불러오기
- 주문 통계

= 1.0.0 =
* Initial Release.

== Upgrade Notice ==
